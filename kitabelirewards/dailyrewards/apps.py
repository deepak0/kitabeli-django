from django.apps import AppConfig


class DailyrewardsConfig(AppConfig):
    name = 'dailyrewards'
