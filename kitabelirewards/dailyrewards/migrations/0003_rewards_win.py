# Generated by Django 3.0.3 on 2020-02-16 15:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dailyrewards', '0002_rewards'),
    ]

    operations = [
        migrations.AddField(
            model_name='rewards',
            name='win',
            field=models.BooleanField(default=False),
        ),
    ]
