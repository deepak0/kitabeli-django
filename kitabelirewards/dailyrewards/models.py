from django.db import models


class User(models.Model):
    name = models.CharField(max_length=20)
    email = models.EmailField(max_length=100)
    password = models.CharField(max_length=20)

    def __str__(self):
        return "name: " + str(self.name) + " email: " + str(self.email)


class Rewards(models.Model):
    email = models.EmailField(max_length=100)
    time = models.DateTimeField()
    win = models.BooleanField(default=False)

    def __str__(self):
        return "email: " + str(self.email) + " time:"+str(self.time) + " win:" + str(self.win)
