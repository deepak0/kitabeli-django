from rest_framework import serializers
from .models import User, Rewards


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['name', 'email', 'password']


class RewardsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rewards
        fields = ['email', 'time', 'win']

