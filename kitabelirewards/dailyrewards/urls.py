"""
daily-rewards app-specific url management
"""
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.home, name="home"),
    path(r'api/register', views.RegisterUser.as_view(), name="register"),
    path(r'api/rewards', views.EnterRewardScheme.as_view(), name="rewards"),
    #path(r'api/login', views.EnterRewardScheme.as_view(), name="login"),
]
