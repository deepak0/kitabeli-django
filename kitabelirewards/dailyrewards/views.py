from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework.generics import GenericAPIView
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from .models import User, Rewards
from .serializers import UserSerializer, RewardsSerializer
from rest_framework.decorators import api_view
from datetime import datetime
from random import random

# Create your views here.


def home(request):
    # return HttpResponse("hello world")
    name = "Deepak"
    contest = "you have entered kitabeli rewards programme"
    contest_result = "You won"
    return render(request, 'home.html',
                  {'name': name, 'contest': contest, 'result': contest_result})


class RegisterUser(GenericAPIView):
    serializer_class = UserSerializer

    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            userObj = User(name=request.data.get('name'),
                           email=request.data.get('email'),
                           password=request.data.get('password'))
            if User.objects.filter(email=userObj.email).exists():
                existingUser = User.objects.get(email=userObj.email)
                if existingUser.password == userObj.password:
                    return Response({"result": True,
                                     "data": {"name": existingUser.name, "email": existingUser.email}},
                                    status=status.HTTP_200_OK)
                else:
                    return Response({"result": False}, status=status.HTTP_400_BAD_REQUEST)
            userObj.save()
            return Response({"result": True,
                             "data": {"name": userObj.name, "email": userObj.email}},
                            status=status.HTTP_200_OK)
        else:
            return Response({"result": False}, status=status.HTTP_400_BAD_REQUEST)


class EnterRewardScheme(GenericAPIView):
    serializer_class = RewardsSerializer

    def post(self, request):
        data = request.data
        time = datetime.now()
        serializer = RewardsSerializer(data={"email": data.get("email"), "time": time, "win": False})
        if serializer.is_valid(raise_exception=True):
            rewardObj = Rewards(email=data.get("email"), time=time, win=False)
            if not User.objects.filter(email=rewardObj.email).exists():
                return Response({"result": False}, status=status.HTTP_400_BAD_REQUEST)
            win = True if (random() > 0.4 and time.second % 2 == 0) else False
            rewardObj.win = win
            rewardObj.save()
            return Response({"result": True,
                             "data": {"win": win, "email": rewardObj.email, "time": rewardObj.time}},
                            status=status.HTTP_200_OK)
        else:
            return Response({"result": False}, status=status.HTTP_400_BAD_REQUEST)


